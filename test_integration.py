import pytest
from app.routes import app
from flask import json, jsonify

@pytest.fixture
def client():
    client = app.test_client()
    yield client

def test_health_check(client):
    response = client.get('/health-check')

def test_single_org_name(client):
    response = client.get('/org/mailchimp/repositories')
    response_json = json.loads(response.data)
    assert response.status_code == 200
    assert 'ruby' in response_json['languages']
