import logging

import flask
from flask import Response
from flask_restplus import Api, fields, Resource

from app.services.git_repos import BitbucketRepoDAO, GithubRepo, merge_repos

app = flask.Flask("user_profiles_api")
logger = flask.logging.create_logger(app)
logger.setLevel(logging.INFO)
api = Api(app, version='1.0', title='Repository API', doc='/docs')

repository_info_model = api.model('RepositoryInfo', {
    'languages': fields.Raw(readonly=True, description='Languages used and how often they\'re used'),
    'topics': fields.Raw(readonly=True, description='Repository taxonomy & distribution'),
    'original': fields.Integer(readOnly=True, description='Total number of public repositories originating from org'),
    'forked': fields.Integer(readOnly=True, description='Total number of forked repositories')
})


class RepositoryDAO(object):
    """
    Data Layer abstraction
    """


    def get(self, org_name):
        return self.get_merge(org_name, org_name)

    def get_merge(self, bitbucket_org_name, github_org_name):
        bitbucket_client = BitbucketRepoDAO(logger)
        github_client = GithubRepo(logger)
        github_client.get_repositories(github_org_name)
        bitbucket_client.get_repositories(bitbucket_org_name)
        return merge_repos(bitbucket_client, github_client)

DAO = RepositoryDAO()


@app.route("/health-check", methods=["GET"])
def health_check():
    """
    Endpoint to health check API
    """
    app.logger.info("Health Check!")
    return Response("All Good!", status=200)


@api.route("/org/<string:org_name>/repositories", methods=["GET"])
@api.response(404, 'Organization not found')
@api.param('org_name', 'The task identifier')
@api.doc(params={'org_name': 'the name of the organization you are querying for'})
class Repositories(Resource):

    @api.doc('list_organization_repositories')
    @api.marshal_with(repository_info_model)
    def get(self, org_name):
        return DAO.get(org_name)

@api.route("/org-merge/<string:bitbucket_org_name>/<string:github_org_name>/repositories", methods=["GET"])
@api.response(404, 'Organization not found')
@api.param('org_name', 'The task identifier')
@api.doc(params={'bitbucket_org_name': 'name of the bitbucket org you are searching for',
                 'github_org_name': 'Name of the github org you are searching for'})
class Repositories(Resource):

    @api.doc('list_organization_repositories')
    @api.marshal_with(repository_info_model)
    def get(self, bitbucket_org_name, github_org_name):
        return DAO.get_merge(bitbucket_org_name, github_org_name)
