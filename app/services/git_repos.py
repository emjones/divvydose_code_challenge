import requests
from functools import reduce
from collections import Counter
import asyncio
from concurrent.futures import ThreadPoolExecutor
import os


def deep_get(dictionary, keys, default=None):
    return reduce(lambda d, key: d.get(key, default) if isinstance(d, dict) else default, keys.split("."), dictionary)


def count_languages(repo, current_counts):
    return count_strings([BitbucketRepoDAO.languages, GithubRepo.languages], repo, current_counts)


def count_topics(repo, current_topics):
    return count_strings([BitbucketRepoDAO.topics, GithubRepo.topics], repo, current_topics)


def count_strings(string_list_getters, repo, current_counts):
    strings = [string for get_string_list in string_list_getters for string in get_string_list(repo)]
    count_dict = Counter(set(strings))
    return count_dict + Counter(current_counts)


def is_original(repo):
    return BitbucketRepoDAO.is_original(repo) or GithubRepo.is_original(repo)


def merge_repos(bitbucket_repo_dao, github_repo_dao):
    info = {
        'languages': {},
        'topics': {},
        'original': 0,
        'forked': 0
    }

    bitbucket_dict = bitbucket_repo_dao.get_name_dict()
    github_dict = github_repo_dao.get_name_dict()

    for key in (list(bitbucket_dict.keys()) + list(github_dict.keys())):
        bitbucket_repo = bitbucket_dict.get(key, {})
        github_repo = github_dict.get(key, {})
        repo = {**bitbucket_repo, **github_repo}
        original = is_original(repo)
        original_count = info['original'] + 1 if original else info['original']
        forked_count = info['forked'] + 1 if original is False else info['forked']
        languages = count_languages(repo, info['languages'])
        topics = count_topics(repo, info['topics'])
        info.update({'languages': dict(languages),
                     'topics': dict(topics),
                     'original': original_count,
                     'forked': forked_count
                     })

    return info


class BitbucketRepoDAO():
    """
    The bitbucket DAO defines the methods for retrieving data from the bitbucket rest API, v2
    """
    BASE_URI = 'https://api.bitbucket.org/2.0'
    REPOSITORIES_URI = 'repositories'

    def __init__(self, logger):
        self.repositories = []
        self.logger = logger

    def get_repositories(self, organization):
        """
        Using the organization string, should recursively call in the presence of a 'next' value in order to handle
        bitbucket's pagination

        Loads repositories into memory
        :param organization: string
        :return: None
        """
        repositories = self._get('{}/{}/{}'.format(self.BASE_URI, self.REPOSITORIES_URI, organization))
        self.repositories = repositories

    def _get(self, link, repositories=[]):
        response = requests.get(link)
        if response.json().get('next') is None:
            return repositories + response.json().get('values')
        else:
            next_repos = self._get(response.json().get('next'))
            return repositories + response.json().get('values') + next_repos

    @staticmethod
    def languages(repo):
        """
        Languages getter for the bitbucket repo key
        :param repo:
        :return:
        """
        return [repo['language']] if repo.get('language') is not None else []

    @staticmethod
    def is_original(repo):
        """
        Falsy check considering repository without a 'parent' one that hasn't been forked
        :param repo:
        :return:
        """
        return repo.get('parent') is None

    @staticmethod
    def topics(repo):
        """
        topics list getter
        :param repo:
        :return:
        """
        return repo.get('topics', [])

    def get_name_dict(self):
        """
        Maps our repositories to their slug name.lower()
        :return:
        """
        return {repo['slug'].lower(): repo for repo in self.repositories if repo.get('slug') is not None}


class GithubRepo():

    BASE_URI = 'http://api.github.com'
    REPOSITORIES_URI = 'repos'
    LANG_URL_LINK = 'languages_url'

    def __init__(self, logger):
        self.repositories = []
        self.logger = logger
        asyncio.set_event_loop(asyncio.new_event_loop())

    def get_repositories(self, organization):
        link = '{}/orgs/{}/{}'.format(self.BASE_URI, organization, self.REPOSITORIES_URI)
        headers = {}
        if (os.environ.get('GITHUB_TOKEN') is not None):
            headers['Authorization'] = 'Bearer {}'.format(os.environ.get('GITHUB_TOKEN'))
        response = requests.get(link, headers=headers)
        if response.status_code in [403, 404, 400, 401]:
            self.logger.error('GITHUB API: {}'.format(response.json()['message']))
            self.repositories = []
        else:
            repositories = response.json()
            loop = asyncio.get_event_loop()
            future = asyncio.ensure_future(get_associated_data(repositories, self.logger))
            loop.run_until_complete(future)
            self.repositories = repositories

    @staticmethod
    def languages(repo):
        return repo.get('languages', [])

    @staticmethod
    def is_original(repo):
        return repo.get('fork', False)

    @staticmethod
    def topics(repo):
        return [label.get('name') for label in repo.get('labels', [])]

    def get_name_dict(self):
        return {repo['name'].lower(): repo for repo in self.repositories if repo.get('name') is not None}


def get_languages_async(session, repo, logger):
    lang_link = repo['languages_url']
    with session.get(lang_link) as response:
        if response.status_code in [403, 404, 400, 401]:
            logger.error('GITHUB API: {}'.format(response.json()['message']))
            return []
        else:
            repo['languages'] = response.json()
            return response.json()


def get_labels_async(session, repo, logger):
    link = repo['labels_url'].replace('{/name}', '')
    with session.get(link) as response:
        if response.status_code in [403, 404, 400, 401]:
            logger.error('GITHUB API: {}'.format(response.json()['message']))
            return []
        else:
            repo['labels'] = response.json()
            return response.json()


async def get_associated_data(repositories, logger):
    with ThreadPoolExecutor(max_workers=10) as executor:
        with requests.Session() as session:
            if(os.environ.get('GITHUB_TOKEN') is not None):
                session.headers['Authorization'] = 'Bearer {}'.format(os.environ['GITHUB_TOKEN'])
            loop = asyncio.get_event_loop()
            tasks = []
            for repo in repositories:
                tasks.append(loop.run_in_executor(
                    executor,
                    get_languages_async,
                    *(session, repo, logger)
                ))
                tasks.append(loop.run_in_executor(
                    executor,
                    get_labels_async,
                    *(session, repo, logger)
                ))

            for response in await asyncio.gather(*tasks):
                pass
    return repositories
