# Coding Challenge App

Em Jones submission to python api code challenge
## Install:

You can use a virtual environment (conda, venv, etc):
```
pip install virtualenv &&
virtualenv venv &&
source venv/bin/activate
```

Install dependencies
``` 
pip install -r requirements.txt
```

## Running the code

Make sure to export a GITHUB_TOKEN environment variable so that the rate limiting
doesn't negatively affect your experience

```bash
$ export GITHUB_TOKEN=123456
```

### Spin up the service

```
# start up local server
python -m run 
```

### Making Requests

See localhost:5000/docs for swagger documentation

### Testing

from root dir run pytest


## What'd I'd like to improve on...

- Testing
- Logging
- Pythonic conventions
- Error handling
- Circuit-breaking