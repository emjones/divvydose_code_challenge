from logging import Logger

from app.services.git_repos import GithubRepo


def test_repo_to_name_hash():
    expected_name1 = 'logical-key'
    expected_url1 = 'https://www.agiantagravic.com'
    expected_name2 = 'wip'
    expected_url2 = 'https://www.emjones.dev'
    github_dao = GithubRepo(Logger('test'))
    github_dao.repositories = [{'name': expected_name1, 'url': expected_url1},
                               {'name': expected_name2, 'url': expected_url2}]
    dictionary = github_dao.get_name_dict()
    assert dictionary.get(expected_name1)['url'] == expected_url1
    assert dictionary.get(expected_name2)['url'] == expected_url2
